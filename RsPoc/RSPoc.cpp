#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <vector>
#include <boost/multiprecision/cpp_int.hpp>

#pragma comment(lib,"ws2_32")

typedef int (WINAPI* SendType)(SOCKET Descriptor,char* Packet,int PacketSize,int Flags);

SendType SendTrampoline;

/*
WS2_32.send - 48 89 5C 24 08        - mov [rsp+08],rbx
WS2_32.send+5- 48 89 6C 24 10        - mov [rsp+10],rbp
WS2_32.send+A- 48 89 74 24 18        - mov [rsp+18],rsi
*/
/* **Replace client modulus with this

49 F5 EB 73 5B 82 9C EB 4B C2 AF 74 64 29 38 A8 AF 7E A4 77 BA 9C 79 B6 9B 5E 65 BC BA 74 84 3E 84 BF 5C D4 D1 F4 EC D4 83 3D C6 9B 7B 52 5C 2F 25 79 6D 21 79 B3 31 7A 0D AD B1 B9 DC 5F E5 3D 13 21 F6 FB 97 1A FB B9 7F 4D 26 0F 10 37 EA EA EC 97 A4 79 37 FB 62 33 9E B3 28 C4 30 8A A6 94 9A 9F 0D DF E2 F5 B4 1F 25 4F E1 6F 35 BF 82 BF E6 A2 A0 15 80 A1 69 97 D8 3D 85 88 9E 88 4D D9

*/
int WINAPI SendHook(SOCKET Descriptor,char* Packet,int PacketSize,int Flags)
{
	if (PacketSize == 353)
	{
		if (Packet[0] == 16)
		{
			MessageBoxA(0, "Attempting to overrwrite key", "", 0);
			char DecryptedBuffer[128] = { 0 };
			char EncryptedBuffer[128] = { 0 };
			char* Offset = Packet + 9;
			std::vector<char> DataToImport;
			for (int Index = 0; Index < 128; ++Index)
				DataToImport.push_back(*(char*)(Offset+Index));

			//Setup
			boost::multiprecision::cpp_int OurModulus("0xd94d889e88853dd89769a18015a0a2e6bf82bf356fe14f251fb4f5e2df0d9f9a94a68a30c428b39e3362fb3779a497eceaea37100f264d7fb9fb1a97fbf621133de55fdcb9b1ad0d7a31b379216d79252f5c527b9bc63d83d4ecf4d1d45cbf843e8474babc655e9bb6799cba77a47eafa838296474afc24beb9c825b73ebf549");
			boost::multiprecision::cpp_int PrivateExponent("0x47b9cfde843176b88741d68cf096952e950813151058ce46f2b048791a26e507a1095793c12bae1e09d82213ad9326928cf7c2350acb19c98f19d32d577d666cd7bb8b2b5ba629d25ccf72a5ceb8a8da038906c84dcdb1fe677dffb2c029fd8926318eede1b58272af22bda5c5232be066839398e42f5352df58848adad11a1");
			boost::multiprecision::cpp_int PublicExponent("0x10001");
			boost::multiprecision::cpp_int RunescapeModulus("0xbc9957dbf566f0daf67d7c8c1409fcb3a2f31f7b7f1e1cc23eb764e4887d864895221ef24941cdf1cb3a88b904f9bbbe425eb45ccf95e2bea788ab489a8323fec25a59c3e2bd9e167234ec621a53154f014771c0be1fd933f4d2ebd1720cb1276de9923139da99a3902c85b9f6585956ffb18a2f54677af6726385c7df8944b1");
			
			//Decrypt with our Exponent
			boost::multiprecision::cpp_int Encrypted;
			boost::multiprecision::import_bits(Encrypted, DataToImport.begin(), DataToImport.begin() + 128);
			boost::multiprecision::cpp_int Decrypted = boost::multiprecision::powm(Encrypted, PrivateExponent, OurModulus);
			boost::multiprecision::export_bits(Decrypted, DecryptedBuffer, 8);

			//Rencrypt with Runescaoe's Modulus
			Encrypted = boost::multiprecision::powm(Decrypted, PublicExponent, RunescapeModulus);
			boost::multiprecision::export_bits(Encrypted, EncryptedBuffer, 8);
			memcpy(Offset, EncryptedBuffer, 128);
		}
	}
	return SendTrampoline(Descriptor,Packet,PacketSize,Flags);
}
LPVOID CreateTrampoline(LPVOID Source, LPVOID Destination, ULONG Size)
{
	LPVOID Trampoline = NULL;
	PBYTE Offset = NULL;
	ULONG Protection = 0;

	if (Size < 12)
		return NULL;

	if (!VirtualProtect(Source, 1, PAGE_EXECUTE_READWRITE, &Protection))
		return NULL;

	Trampoline = malloc(Size + 12);
	
	if (!Trampoline)
		return NULL;

	if (!VirtualProtect(Trampoline, 1, PAGE_EXECUTE_READWRITE, &Protection))
		goto Cleanup;

	memcpy(Trampoline, Source, Size);

	Offset = (PBYTE)Trampoline;

	Offset += Size;
	
	*(PBYTE)(Offset + 0) = 0x48;
	*(PBYTE)(Offset + 1) = 0xB8;
	Offset += 2;
	*(PDWORD64)(Offset + 0) = (DWORD64)Source+12;
	Offset += 8;
	*(PBYTE)(Offset + 0) = 0xFF;
	*(PBYTE)(Offset + 1) = 0xE0;

	memset(Source, 0x90, Size);

	Offset = (PBYTE)Source;
	
	*(PBYTE)(Offset + 0) = 0x48;
	*(PBYTE)(Offset + 1) = 0xB8;
	Offset += 2;
	*(PDWORD64)(Offset + 0) = (DWORD64)Destination;
	Offset += 8;
	*(PBYTE)(Offset + 0) = 0xFF;
	*(PBYTE)(Offset + 1) = 0xE0;

	return Trampoline;

Cleanup:
	if (Trampoline)
		free(Trampoline);

	return NULL;
}
ULONG CALLBACK CreateHook(LPVOID Context)
{
	SendTrampoline = (SendType)CreateTrampoline(GetProcAddress(GetModuleHandleW(L"WS2_32"), "send"), SendHook, 15);
	return 1;
}
BOOL APIENTRY DllMain
(
	HINSTANCE Instance,
	ULONG Reason,
	LPVOID Reserved
)
{
	DisableThreadLibraryCalls(Instance);
	switch (Reason)
	{
	case DLL_PROCESS_ATTACH:
	{
		//CreateThread
		//(
		//	NULL,
		//	0,
		//	CreateHook,
		//	Instance,
		//	0,
		//	NULL
		//);
		CreateHook(NULL);
	}
	break;
	}
	return TRUE;
}